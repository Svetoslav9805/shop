package com.springboot.shop.payload;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductUpdateRequestDTO {
    private String name;
    private String description;
    private BigDecimal price;

}