package com.springboot.shop.payload;

import com.springboot.shop.enums.CartStatus;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class CartDto {
    private Long id;
    private List<ItemDto> items;
    private BigDecimal totalPrice;
    private CartStatus status;

    // Constructors
    public CartDto() {
    }

    public CartDto(Long id, List<ItemDto> items, BigDecimal totalPrice, CartStatus status) {
        this.id = id;
        this.items = items;
        this.totalPrice = totalPrice;
        this.status = status;
    }
}