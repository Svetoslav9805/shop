package com.springboot.shop.enums;

public enum OrderStatus {
    PENDING,
    SHIPPED,
    DELIVERED
}
