package com.springboot.shop.service;

import com.springboot.shop.entity.Image;

public interface ImageService {
    Image getImageById (int id);
    Image getImageByProductId(int productId);
    Image createImage(Image image);
    String deleteImage(int id);

}
