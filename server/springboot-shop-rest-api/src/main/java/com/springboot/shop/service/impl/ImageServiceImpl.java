package com.springboot.shop.service.impl;

import com.springboot.shop.entity.Image;
import com.springboot.shop.repository.ImageRepository;
import com.springboot.shop.service.ImageService;

public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;

    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Image getImageById(int id) {
        return null;
    }

    @Override
    public Image getImageByProductId(int productId) {
        return null;
    }

    @Override
    public Image createImage(Image image) {
        Image savedImage = imageRepository.save(image);
        return savedImage;
    }

    @Override
    public String deleteImage(int id) {
        return null;
    }
}
