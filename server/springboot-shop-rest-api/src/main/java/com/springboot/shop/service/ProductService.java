package com.springboot.shop.service;

import com.springboot.shop.payload.ProductDto;
import com.springboot.shop.payload.ProductResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

public interface ProductService {
    public ProductDto createProduct(ProductDto productDto);

    public ProductResponse getAllProducts(int pageNo, int pageSize, String sortBy, String sortDir);

    public List<ProductDto> getAllProductsSortedByCategory(String name);

    public List<ProductDto> getProductsByPriceRange(double minPrice, double maxPrice);


    public ProductDto getProductById(long id);

    public ProductDto updateProduct(ProductDto productDto, long id);

    public void deleteProductById(long id);
}
