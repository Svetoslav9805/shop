package com.springboot.shop.service.impl;

import com.springboot.shop.entity.Cart;
import com.springboot.shop.entity.CartItem;
import com.springboot.shop.entity.Product;
import com.springboot.shop.exception.ResourceNotFoundException;
import com.springboot.shop.repository.CartItemRepository;
import com.springboot.shop.repository.CartRepository;
import com.springboot.shop.repository.ProductRepository;
import com.springboot.shop.service.CartService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
@Service
public class CartServiceImpl implements CartService {

    private CartRepository cartRepository;
    private CartItemRepository cartItemRepository;

    private ProductRepository productRepository;

    public CartServiceImpl(CartRepository cartRepository, CartItemRepository cartItemRepository, ProductRepository productService) {
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
        this.productRepository = productService;
    }

    @Override
    public void addToCart(Long cartId, Long productId, int quantity) {
        Cart cart = cartRepository
                .findById(cartId)
                .orElseThrow(() -> new ResourceNotFoundException("Cart", "id", cartId));

        Product product = productRepository
                .findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));

        CartItem cartItem = new CartItem();
        cartItem.setCart(cart);
        cartItem.setProduct(product);
        cartItem.setQuantity(quantity);
        cartItem.setPrice(product.getPrice().multiply(BigDecimal.valueOf(quantity)));
        cart.getCartItems().add(cartItem);
        cart.setTotalPrice(cart.getTotalPrice().add(cartItem.getPrice()));
        cartRepository.save(cart);

    }

    @Override
    public void removeFromCart(Long cartId, Long cartItemId) {
        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new ResourceNotFoundException("Cart", "id", cartId));

        CartItem cartItem = cartItemRepository.findById(cartId)
                .orElseThrow(() -> new ResourceNotFoundException("CartItem", "id", cartItemId));

        if (!cart.getCartItems().contains(cartItem)) {
            throw new ResourceNotFoundException("cartItem", "id", cartItemId);
        }
        cart.getCartItems().remove(cartItem);
        cart.setTotalPrice(cart.getTotalPrice().subtract(cartItem.getPrice()));
        cartRepository.save(cart);
    }
}
