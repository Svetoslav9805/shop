package com.springboot.shop.payload;

import com.springboot.shop.entity.OrderItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ItemDto {
    private Long id;
    private ProductDto productDto;
    private int quantity;

    public ItemDto(OrderItem item) {
        this.id = item.getId();
        this.productDto = new ProductDto(item.getProduct());
        this.quantity = item.getQuantity();
    }

}