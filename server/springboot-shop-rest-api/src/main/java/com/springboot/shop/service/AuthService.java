package com.springboot.shop.service;

import com.springboot.shop.payload.LoginDto;
import com.springboot.shop.payload.RegisterDto;

public interface AuthService {
    String login(LoginDto loginDto);
    String register(RegisterDto registerDto);

}