package com.springboot.shop.utils;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.springboot.shop.entity.Image;
import com.springboot.shop.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Component
public class UploadImageHelper {
    @Value("${cloudinary.cloud_name}")
    private static String cloudName;

    @Value("${cloudinary.api_key}")
    private static String apiKey;

    @Value("${cloudinary.api_secret}")
    private static String apiSecret;
    private static ImageService imageService;


    @Autowired
    public UploadImageHelper(ImageService imageService) {
        this.imageService = imageService;
    }

    public static Image uploadCloudinary(MultipartFile multipartFile) throws IOException {
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret));
        Map upload_result = null;
        upload_result = cloudinary.uploader().cloudinary().uploader().upload(multipartFile.getBytes(),ObjectUtils.emptyMap());
        Image image = new Image();
        image.setImage_link(upload_result.get("url").toString());
        imageService.createImage(image);
        return image;
    }


}
