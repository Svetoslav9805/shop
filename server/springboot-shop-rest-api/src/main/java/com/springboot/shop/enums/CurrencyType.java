package com.springboot.shop.enums;

public enum CurrencyType {
    BGN, EUR, USD
}
