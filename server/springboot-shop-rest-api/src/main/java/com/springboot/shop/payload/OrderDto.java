package com.springboot.shop.payload;

import com.springboot.shop.enums.OrderStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
@Data
public class OrderDto {
    private Long id;
    private LocalDateTime dateCreated;
    private BigDecimal totalPrice;
    private OrderStatus status;
    private List<ItemDto> items;
    private UserDto userDto;
}
