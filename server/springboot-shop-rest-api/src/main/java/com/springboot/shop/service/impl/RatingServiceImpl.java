package com.springboot.shop.service.impl;

import com.springboot.shop.entity.Product;
import com.springboot.shop.entity.Rating;
import com.springboot.shop.payload.RatingDto;
import com.springboot.shop.repository.RatingRepository;
import com.springboot.shop.service.RatingService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class RatingServiceImpl implements RatingService {

    private RatingRepository ratingRepository;

    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public void addRating(RatingDto ratingDto) {
        Rating rating = new Rating();
        rating.setRatingValue(ratingDto.getRatingValue());

        Product product = new Product();
        product.setId(ratingDto.getProductId());
        rating.setProduct(product);

        ratingRepository.save(rating);
    }

    @Override
    public List<RatingDto> getRatingsByProductId(Long productId) {
        List<Rating> ratings = ratingRepository.findByProductId(productId);
        return ratings.stream()
                .map(rating -> {
                    RatingDto ratingDto = new RatingDto();
                    ratingDto.setId(rating.getId());
                    ratingDto.setRatingValue(rating.getRatingValue());
                    return ratingDto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public double getAverageRatingByProductId(Long productId) {
        return ratingRepository.avgRatingByProductId(productId);
    }
}
