package com.springboot.shop.utils.mapper;

import com.springboot.shop.entity.Order;
import com.springboot.shop.entity.User;
import com.springboot.shop.payload.OrderDto;
import com.springboot.shop.payload.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private ModelMapper modelMapper;

    public UserMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public User mapUserDtoToEntity(UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        return user;
    }

    public UserDto mapUserToDTO(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }
}
