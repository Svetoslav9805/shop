package com.springboot.shop.payload;

import com.springboot.shop.entity.Image;
import com.springboot.shop.entity.Product;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@Setter
@NoArgsConstructor
public class ProductDto {
    private Long id;
    @NotEmpty
    @Size(min = 5, message = "Name must be at least 5 characters")
    private String name;
    @NotEmpty
    @Size(min = 10, message = "description must be at least 10 characters")
    private String description;
    @NotEmpty
    private BigDecimal price;

    @NotEmpty
    private Image photo;

    public ProductDto(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.photo = product.getPhoto();
    }
}