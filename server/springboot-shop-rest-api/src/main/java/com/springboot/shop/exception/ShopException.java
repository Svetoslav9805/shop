package com.springboot.shop.exception;

import org.springframework.http.HttpStatus;

public class ShopException extends RuntimeException{
    private HttpStatus status;
    private String message;

    public ShopException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public ShopException(String message, HttpStatus status, String message1) {
        super(message);
        this.status = status;
        this.message = message1;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
