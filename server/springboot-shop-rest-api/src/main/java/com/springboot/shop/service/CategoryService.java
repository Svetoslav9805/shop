package com.springboot.shop.service;

import com.springboot.shop.payload.CategoryDto;

public interface CategoryService {

    public CategoryDto getCategoryById(Long id);

    public CategoryDto createCategory(CategoryDto categoryDto);

    public void removeCategoryById(Long id);
}
