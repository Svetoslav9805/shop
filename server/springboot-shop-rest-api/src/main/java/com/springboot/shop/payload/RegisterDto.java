package com.springboot.shop.payload;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {

    @NotEmpty(message = "Name can't be empty")
    @Size(min = 5, max = 30, message = "name must between 5 and 30 symbols long")
    private String name;
    @NotEmpty(message = "username can't be empty")
    @Size(min = 2, max = 20, message = "username must between 2 and 20 symbols long")
    private String username;
    @NotEmpty(message = "Email can't be empty")
    @Email
    private String email;

    @NotEmpty
    @Size(min = 8,max = 20,message = "Password must be between 8 and 20 symbols")
    private String password;
    private String phone;

}