package com.springboot.shop.controller;

import com.springboot.shop.payload.RatingDto;
import com.springboot.shop.service.RatingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/ratings")
public class RatingController {
    private RatingService ratingService;

    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @PostMapping
    public ResponseEntity<Void> addRating(@RequestBody RatingDto ratingDto) {
        ratingService.addRating(ratingDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{productId}")
    public ResponseEntity<List<RatingDto>> getRatingsByProductId(@PathVariable Long productId) {
        List<RatingDto> ratingDtos = ratingService.getRatingsByProductId(productId);
        return ResponseEntity.ok(ratingDtos);
    }

    @GetMapping("/{productId}/average")
    public ResponseEntity<Double> getAverageRatingByProductId(@PathVariable Long productId) {
        double averageRating = ratingService.getAverageRatingByProductId(productId);
        return ResponseEntity.ok(averageRating);
    }
}