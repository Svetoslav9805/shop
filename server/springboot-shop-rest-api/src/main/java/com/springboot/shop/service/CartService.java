package com.springboot.shop.service;

public interface CartService {
    public void addToCart(Long cartId, Long productId, int quantity);

    public void removeFromCart(Long cartId, Long cartItemId);
}
