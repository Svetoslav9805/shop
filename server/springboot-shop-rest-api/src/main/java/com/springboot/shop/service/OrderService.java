package com.springboot.shop.service;

import com.springboot.shop.entity.Order;
import com.springboot.shop.entity.OrderItem;
import com.springboot.shop.entity.User;
import com.springboot.shop.payload.OrderDto;

import java.math.BigDecimal;
import java.util.Set;

public interface OrderService {
    public OrderDto createOrder(OrderDto orderDto);
}
