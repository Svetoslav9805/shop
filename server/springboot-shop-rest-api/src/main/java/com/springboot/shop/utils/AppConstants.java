package com.springboot.shop.utils;

public class AppConstants {
    public static final String DEFAULT_PAGE_NUMBER = "0";
    public  static final String DEFAULT_PAGE_SIZE = "10";
    public static final String DEFAULT_SORT_BY = "id";
    public static final String DEFAULT_SORT_DIRECTION = "asc";

    public static final String DEFAULT_USER_ROLE="ROLE_CUSTOMER";
    public static final String USER_ADMIN_ROLE="ROLE_ADMIN";
}
