package com.springboot.shop.payload;

import lombok.Data;

@Data
public class RatingDto {
    private Long id;
    private int ratingValue;

    private Long productId;
}
