package com.springboot.shop.enums;

public enum CartStatus {
    OPEN,
    CHECKOUT,
    PAID,
    CANCELLED
}
