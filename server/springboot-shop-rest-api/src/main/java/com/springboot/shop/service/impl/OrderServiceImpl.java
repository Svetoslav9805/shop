package com.springboot.shop.service.impl;

import com.springboot.shop.entity.Order;
import com.springboot.shop.entity.OrderItem;
import com.springboot.shop.enums.OrderStatus;
import com.springboot.shop.payload.ItemDto;
import com.springboot.shop.payload.OrderDto;
import com.springboot.shop.repository.OrderRepository;
import com.springboot.shop.service.OrderService;
import com.springboot.shop.utils.mapper.OrderMapper;
import com.springboot.shop.utils.mapper.ProductMapper;
import com.springboot.shop.utils.mapper.UserMapper;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
    private OrderRepository orderRepository;
    private OrderMapper orderMapper;
    private ProductMapper productMapper;

    private UserMapper userMapper;

    public OrderServiceImpl(OrderRepository orderRepository, OrderMapper orderMapper,ProductMapper productMapper, UserMapper userMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.productMapper = productMapper;
        this.userMapper = userMapper;
    }


    public OrderDto createOrder(OrderDto orderDto) {
        Order order = new Order();
        order.setOrderDate(LocalDateTime.now());
        order.setOrderStatus(OrderStatus.PENDING);
        order.setUser(userMapper.mapUserDtoToEntity(orderDto.getUserDto()));

        Set<OrderItem> items = new HashSet<>();
        for (ItemDto itemDto : orderDto.getItems()) {
            OrderItem orderItem = new OrderItem();
            orderItem.setProduct(productMapper.mapProductDtoToEntity(itemDto.getProductDto()));
            orderItem.setQuantity(itemDto.getQuantity());
            orderItem.setPrice(itemDto.getProductDto().getPrice());
            orderItem.setOrder(order);
            items.add(orderItem);
        }
        order.setOrderItems(items);

        BigDecimal totalPrice = items.stream()
                .map(item -> item.getPrice().multiply(BigDecimal.valueOf(item.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        order.setTotal(totalPrice);

        Order savedOrder = orderRepository.save(order);
        return orderMapper.mapToDTO(savedOrder);
    }


}
