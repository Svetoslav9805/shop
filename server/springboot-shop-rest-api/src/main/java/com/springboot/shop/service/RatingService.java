package com.springboot.shop.service;

import com.springboot.shop.payload.RatingDto;

import java.util.List;

public interface RatingService {
    void addRating(RatingDto ratingDto);
    List<RatingDto> getRatingsByProductId(Long productId);
    double getAverageRatingByProductId(Long productId);
}
