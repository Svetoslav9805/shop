package com.springboot.shop.repository;

import com.springboot.shop.entity.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating,Long> {
    List<Rating> findByProductId(Long productId);
    @Query("SELECT AVG(r.ratingValue) FROM Rating r WHERE r.product.id = :productId")
    double avgRatingByProductId(Long productId);
}
